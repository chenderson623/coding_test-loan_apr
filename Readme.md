#Coding Test

This is a coding test I was recently given by a company I was applying to.

I created a repo for it because it is a good problem. A good example of how to take a small, complex problem and abstract it out to the basis for additional extension.

##The Original Problem
```
Please complete the function below, in PHP.  The goal is to create a function that calculates APR when provided with Interest Rate, Origination Date, Payment Schedule (array of payment dates) and Loan Amount.

This function must be able to calculate an APR for installment loans with irregular periods, meaning the length of time between each payment is not always a full interest period.  Often, each payment is a few days more or less than a whole interest period from the start of the loan. To solve for the APR, you will need to utilize an iterative approach. Your equation should start with a variable that contains an estimated APR. You will then continue solving the APR equation and adjusting the estimated APR until your estimated APR converges on the correct answer (Y(i) = 0).

There is partially completed PHP code below, you can copy it into the edit box and make your changes.  The code is missing one section that iterates through the APR function to find the correct value. Also, there are some structural issues with the code below and it’s not set up in an object-oriented way.

You should complete the problem below and provide clean, well-structured, well-commented code. (The editor defaults to Python, you probably want to change it over to PHP)

//————————————————
$interest_rate = 6.5;
$origination_date="1/25/2018";
$payment_schedule=array();
$payment_schedule[1] = '2018-02-02';
$payment_schedule[2] = '2018-02-16';
$payment_schedule[3] = '2018-03-02';
$payment_schedule[4] = '2018-03-16';
$payment_schedule[5] = '2018-03-30';
$payment_schedule[6] = '2018-04-13';
$payment_schedule[7] = '2018-04-27';
$payment_schedule[8] = '2018-05-11';
$payment_schedule[9] = '2018-05-25';
$payment_schedule[10] = '2018-06-08';
$payment_schedule[11] = '2018-06-22';
$payment_schedule[12] = '2018-07-06';


/**
* Code here to iterate through testing different apr values until we converge on the right answer. Right answer is: 7.3303
*
*/

//correct example - (print(round(apr(7.3303,$interest_rate,$payment_schedule,$origination_date),2));


function apr($apr_guess,$interest_rate,$payment_schedule,$origination_date)
{
    $whole_periods = [];
    $extra_days = array();
    $loan_amount = 300;
    $number_of_payments = count($payment_schedule);
    $interest_periods = 26;
    $unit_period = 14;
    $payment_amount = $loan_amount * (($interest_rate / $interest_periods) / (1 - pow((1 + ($interest_rate / $interest_periods)), -$number_of_payments)));
    $period_apr = $apr_guess / (365 / $unit_period);

    for ($x = 1; $x <= $number_of_payments; $x++) {
        $whole_periods[$x] = $x;
        $extra_days[$x] = (strtotime(date($payment_schedule[$x])) - strtotime($origination_date . " + " . $unit_period * ($x) . " days")) / (86400);

        while ($extra_days[$x] > $unit_period) {
            $extra_days[$x] = $extra_days[$x] - $unit_period;
            $whole_periods[$x]++;
        }
        while ($extra_days[$x] < 0) {
            $extra_days[$x] = $extra_days[$x] + $unit_period;
           $whole_periods[$x]--;
        }
    }

    $calculated_apr = -$loan_amount;
    for ($i = 1; $i <= $number_of_payments; $i++) {
        $calculated_apr += $payment_amount / ((1 + (($extra_days[$i] / $unit_period) * $period_apr)) * (pow((1 + $period_apr), $whole_periods[$i])));
    }

    return $calculated_apr;

}
//————————————————
```

## First Submission
My first submission is included here: https://bitbucket.org/chenderson623/coding_test-loan_apr/src/master/1st-submission/submission.php

It is not a very good solution. I was given an hour and a half to complete the test. I was thrown off by the given problem. I spent too much time at the beginning trying to figure out the given formulas, and what exactly was being calculated. Instead of addressing the first part of the problem, which was to implement an iterator, I started to break down the code into classes.

The iterator I came up with (Loan::calculateApr()) is a naive, brute force solution. It worked, deriving the correct answer in 24 iterations.

## Second Submission
I don't think the employer was very impressed by my first submission, but I was fortunate to be invited for a second interview. I decided to take advantage and prepared a second submission, on the chance that I might have an opportunity to present it. I was able to study the problem and develop a better solution without the time constraint.

The first thing I did was to improve the iterator. (https://bitbucket.org/chenderson623/coding_test-loan_apr/src/master/step1-completed-problem.php) This was able to provide the correct answer after 4 iterations.

The second thing I did was to analyze the formulas and use the iterator to trace out what was going on. I also researched financial formulas to try to identify standard formulas. Here is my derivation: https://bitbucket.org/chenderson623/coding_test-loan_apr/src/master/step2-derivation.php

The full solution is here: https://bitbucket.org/chenderson623/coding_test-loan_apr/src/master/2nd-submission/

It is an over-thought solution for what was asked, but I was looking to demonstrate an ability to break a problem down to smaller units, and hopefully in a way that could be expanded upon to solve additional problems. You will see sprinkled throughout the code some unknowns and questions I still have (I do not have a financial background).

###Components
* **Loan:** loan consists of a loan amount, an interest object, and a payment schedule object
    - attempted to encapsulate what consists a loan, and delegate out what can change
    - Loan should be a representation of a loan, but not get bogged down with financial calculations
* **LoanInterest:** component of a Loan. Allows the use of other interest types in a Loan
    - encapsulates what can change in an interest calculation: interest type (simple, compounding, other), other components that affect interest: compounding period, points
    - uses LoanInterest and PaymentSchedule interfaces as strategies
* **PaymentSchedule:** Holds our calculated payment period intervals
    - encapsulates the calculation of payment period whole periods/partial periods. A normal period is defined here
* **FinancialFormula:** Our financial calculation library. Separated for use in other code
* **PaymentPeriodFactory:** Re-usable code to generate PaymentPeriod objects
    - PayPeriods COULD calculate cumulative periods themselves, but each would have to know the origination date and days in a normal period APRCalculation: This is the meat of the problem. Calculates actual APR by interpolating from an initial guess. Tests a given APR by calculating payments to principal and iterating until the calculated payments equal the loan's loan amount
* **Iterator:** A generic iterator that should be reusable

###Possible Improvements
1. Here we are putting origination date in the PaymentSchedule. Needed to calculate period days, but doesn't logically fit as a "payment". PaymentSchedule might be re-nameable to something like "LoanSchedule"
2. Creation of APRCalculation is hard-coded in Loan. Might be better to inject it as a dependency to Loan.
  * Problem is that APRCalculation is dependent on both LoanInterest and PaymentSchedule.
  * The calculation made in APRCalculation is probably invalid for other LoanInterest types.
3. Other loan parameters affect APR. Like: fees, points
4. Need to improve error checking. Throw custom Exceptions
