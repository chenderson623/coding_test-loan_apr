<?php

class Loan
{
    protected $loan_amount;
    protected $interest_rate;
    protected $payment_schedule;

    public function __construct($loan_amount, $interest_rate, PaymentSchedule $payment_schedule)
    {
        $this->loan_amount      = $loan_amount;
        $this->interest_rate    = $interest_rate;
        $this->payment_schedule = $payment_schedule;
    }

    public function numberOfPayments()
    {
        return $this->payment_schedule->numberOfPayments();
    }

    public function paymentAmount()
    {
        return $this->payment_schedule->paymentAmount($this->loan_amount, $this->interest_rate);
    }

    public function calculatedAprDiff($apr_guess)
    {
        $payment_amount = $this->paymentAmount();
        $period_apr  = $this->payment_schedule->periodApr($apr_guess, 365);
        $unit_period = $this->payment_schedule->unitPeriod();

        $apr_diff = -$this->loan_amount;
        foreach ($this->payment_schedule->paymentPeriods() as $payment_period) {
            //TODO: need descriptive name:
            $x = ($payment_period->extra_days / $unit_period) * $period_apr;
            //TODO: need descriptive name:
            $y = pow((1 + $period_apr), $payment_period->whole_periods);
            $apr_diff += $payment_amount / ((1 + $x) * $y);
        }

        return $apr_diff;
    }

    public function calculateApr()
    {
        // Start with our loan's interest rate as first guess
        // TODO: mathimatically, can we get closer?
        $apr    = $this->interest_rate;
        $count = 0;
        do {
            $diff = $this->calculatedAprDiff($apr);
            $change_direction = $diff < 0 ? -1 : 1;
            $change = $change_direction * ($diff * .01);

            $count++;

            $apr = $apr + $change;
        } while (round($diff, 2) > 0);
        // for debug:
        //var_dump("ITERATIONS: $count");
        return round($apr, 4);
    }
}

class PaymentSchedule
{
    // TODO: do these need to be passed? Might be something that can be gotten from the payment dates array
    protected $unit_period;
    protected $interest_periods;

    protected $origination_date;
    protected $payment_periods;

    public function __construct($unit_period, $interest_periods, $origination_date, array $payment_dates)
    {
        $this->unit_period      = $unit_period;
        $this->interest_periods = $interest_periods;
        $this->origination_date = $origination_date;
        $this->payment_periods  = $this->createPaymentPeriods($payment_dates);
    }

    public function paymentPeriods()
    {
        return $this->payment_periods;
    }

    public function unitPeriod()
    {
        return $this->unit_period;
    }

    public function numberOfPayments()
    {
        return count($this->payment_periods);
    }

    public function paymentAmount($loan_amount, $interest_rate)
    {
        $interest_per_period = $interest_rate / $this->interest_periods;
        //TODO: need descriptive name:
        $x = pow((1 + $interest_per_period), -$this->numberOfPayments());
        return $loan_amount * ($interest_per_period / (1 - $x));
    }

    // TODO: don't like this. Is this the appropriate place to calc interest rate
    // have this here because this class hass unit_period
    public function periodApr($apr, $days)
    {
        return $apr / ($days / $this->unit_period);
    }

    protected function createPaymentPeriods($payment_dates)
    {
        $payment_factory = new PaymentPeriodFactory($this->origination_date, $this->unit_period);
        return $payment_factory->paymentPeriods($payment_dates);
    }
}

class PaymentPeriod
{
    public $payment_date;
    public $whole_periods;
    public $extra_days;

    public function __construct($payment_date, $whole_periods, $extra_days)
    {
        $this->payment_date = $payment_date;
        $this->whole_periods = $whole_periods;
        $this->extra_days = $extra_days;
    }
}

class PaymentPeriodFactory
{
    protected $origination_date;
    protected $unit_period;

    public function __construct($origination_date, $unit_period)
    {
        $this->origination_date = $origination_date;
        $this->unit_period      = $unit_period;
    }

    protected function extraDaysForPeriod($period_number, $payment_date)
    {
        $full_period_end   = strtotime($this->origination_date . " + " . $this->unit_period * ($period_number) . " days");
        $actual_period_end = strtotime(date($payment_date));
        // return number of days
        return ($actual_period_end - $full_period_end) / (86400);
    }

    public function paymentPeriod($period_number, $payment_date)
    {
        $whole_periods = $period_number;
        $extra_days    = $this->extraDaysForPeriod($period_number, $payment_date);

        // have enough days for another period:
        while ($extra_days > $this->unit_period) {
            $extra_days = $extra_days - $this->unit_period;
            $whole_periods++;
        }
        // negative extra days: take from whole period
        while ($extra_days < 0) {
            $extra_days = $extra_days + $this->unit_period;
            $whole_periods--;
        }
        return new PaymentPeriod($payment_date, $whole_periods, $extra_days);
    }

    public function paymentPeriods($payment_schedule)
    {
        $payment_periods = [];
        foreach ($payment_schedule as $payment_num => $payment_date) {
            $payment_periods[] = $this->paymentPeriod($payment_num, $payment_date);
        }
        return $payment_periods;
    }
}


/*
 *
 * Use Case:
 *
*/

$unit_period = 14;
$interest_periods = 26;
$origination_date = "1/25/2018";
$payment_schedule = [];
$payment_schedule[1] = '2018-02-02';
$payment_schedule[2] = '2018-02-16';
$payment_schedule[3] = '2018-03-02';
$payment_schedule[4] = '2018-03-16';
$payment_schedule[5] = '2018-03-30';
$payment_schedule[6] = '2018-04-13';
$payment_schedule[7] = '2018-04-27';
$payment_schedule[8] = '2018-05-11';
$payment_schedule[9] = '2018-05-25';
$payment_schedule[10] = '2018-06-08';
$payment_schedule[11] = '2018-06-22';
$payment_schedule[12] = '2018-07-06';

// Define our payment periods. Do this once, instead of on each iteration:
$payment_schedule = new PaymentSchedule($unit_period, $interest_periods, $origination_date, $payment_schedule);

// Define out loan:
$loan_amount = 300;
$interest_rate = 6.5;
$loan = new Loan($loan_amount, $interest_rate, $payment_schedule, $unit_period, $interest_periods);

// Define an initial guess:
$apr = $loan->calculateApr();

print $apr . PHP_EOL;
