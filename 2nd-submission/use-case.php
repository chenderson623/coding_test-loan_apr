<?php

namespace LoanCodingTest;

use DateTimeImmutable;

require('vendor/autoload.php');

/*
 *
 * Use Case:
 *
*/


// Define our parameters:
$loan_amount = 300;
$interest_rate = 6.5;
$origination_date = new DateTimeImmutable("2018-01-25");
$payment_dates = [
    new DateTimeImmutable('2018-02-02'),
    new DateTimeImmutable('2018-02-16'),
    new DateTimeImmutable('2018-03-02'),
    new DateTimeImmutable('2018-03-16'),
    new DateTimeImmutable('2018-03-30'),
    new DateTimeImmutable('2018-04-13'),
    new DateTimeImmutable('2018-04-27'),
    new DateTimeImmutable('2018-05-11'),
    new DateTimeImmutable('2018-05-25'),
    new DateTimeImmutable('2018-06-08'),
    new DateTimeImmutable('2018-06-22'),
    new DateTimeImmutable('2018-07-06')
];

$loan = new Loan(
    $loan_amount,
    new AnnualInterestCompoundedBiWeekly($interest_rate),
    new BiWeeklyPaymentSchedule($origination_date, $payment_dates)
);

// To see the full iterator result:
$apr_calculation = new APRCalculation(
    $loan,
    new AnnualInterestCompoundedBiWeekly($interest_rate),
    new BiWeeklyPaymentSchedule($origination_date, $payment_dates)
);
var_dump($apr_calculation->aprIteratorResult());

$apr = $loan->calculatedApr();

print $apr . PHP_EOL;
