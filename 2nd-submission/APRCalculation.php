<?php

namespace LoanCodingTest;

/**
 * Boils APR calculation down to a formula with one argument that can be passed to the iterator
 *
 * This class does not need to know about the composition of Loan
 *
 * it may be beneficial to be able to test against various PaymentSchedules
 * or LoanInterests without modifying the loan
 */
class APRCalculation
{
    protected $loan;
    protected $loan_interest;
    protected $payment_schedule;

    protected $apr_precision = 4;

    public function __construct(Loan $loan, LoanInterest $loan_interest, PaymentSchedule $payment_schedule)
    {
        $this->loan             = $loan;
        $this->loan_interest    = $loan_interest;
        $this->payment_schedule = $payment_schedule;
    }

    public function apr()
    {
        $iterator_result = $this->aprIteratorResult();

        return round($iterator_result->answer, $this->apr_precision);
    }

    /**
     * @return     IteratorResult
     */
    public function aprIteratorResult()
    {
        // Use our loan's interest rate for our first guess
        $first_guess = $this->loan_interest->annualInterestRate();

        $iterator = new Iterator($this->apr_precision, 0.1);

        $function = [$this, 'calculatePrincipalOverpaymentForApr'];

        $desired_answer = 0;

        $result = $iterator->iterate($function, $desired_answer, $first_guess);

        return $result;
    }

    /**
     * This is the function we are going to pass to the iterator
     *
     * @param      float   $apr
     *
     * @return     float  The total principal repaid to the loan at the APR given.
     */
    public function calculatePrincipalPaymentForApr($apr)
    {
        // Note: would LIKE to be able to call this->loanInterest()->annunualInterestPeriods() here but
        // the original problem uses this. Repeating here to get the same answer as the original problem.
        $rate_per_period_for_apr = FinancialFormula::ratePerPeriod($apr, 365 / $this->payment_schedule->daysInPeriod());

        $total_principal_payments = 0;
        for ($i = 1; $i <= $this->payment_schedule->numberOfPeriods(); $i++) {
            $total_principal_payments+= FinancialFormula::amortizedPrincipalForPeriod(
                $this->loan->paymentAmount(),
                $rate_per_period_for_apr,
                $this->payment_schedule->cumulativePeriodsForPeriod($i),
                $this->payment_schedule->cumulativePartialPeriodRatioForPeriod($i)
            );
        }
        return $total_principal_payments;
    }

    /**
     * This method reproduces the result from our original problem
     *
     * @param      float  $apr    APR guess
     */
    public function calculatePrincipalOverpaymentForApr($apr)
    {
        return $this->calculatePrincipalPaymentForApr($apr) - $this->loan->loanAmount();
    }
}
