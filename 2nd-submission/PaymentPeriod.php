<?php

namespace LoanCodingTest;

use DateTimeImmutable;

class PaymentPeriod
{
    protected $period_number;
    protected $payment_date;
    protected $cumulative_periods;
    protected $cumulative_partial_days;

    public function __construct($period_number, DateTimeImmutable $payment_date, $whole_periods, $partial_days)
    {
        $this->period_number           = (int) $period_number;
        $this->payment_date            = $payment_date;
        $this->cumulative_periods      = $whole_periods;
        $this->cumulative_partial_days = $partial_days;
    }

    public function periodNumber()
    {
        return $this->period_number;
    }

    public function paymentDate()
    {
        return $this->payment_date;
    }

    public function cumulativePeriods()
    {
        return $this->cumulative_periods;
    }

    public function cumulativePartialDays()
    {
        return $this->cumulative_partial_days;
    }
}
