<?php

namespace LoanCodingTest;

use DateTimeImmutable;

/**
 * Payments due every 2 weeks (14 days)
 */
class BiWeeklyPaymentSchedule implements PaymentSchedule
{
    protected $days_in_normal_period = 14;

    protected $origination_date;
    protected $payment_dates;

    protected $payment_periods;

    public function __construct(DateTimeImmutable $origination_date, array $payment_dates)
    {
        $this->origination_date = $origination_date;
        //TODO: should validate that payment_dates is an array of DateTimeImmutable
        $this->payment_dates    = $payment_dates;
    }

    public function paymentPeriods()
    {
        // Lazy-generate payment periods
        if (!isset($this->payment_periods)) {
            $factory = new PaymentPeriodFactory($this->origination_date, $this->days_in_normal_period);
            $this->payment_periods = $factory->paymentPeriods($this->payment_dates);
        }
        return $this->payment_periods;
    }

    public function daysInPeriod()
    {
        return $this->days_in_normal_period;
    }

    public function paymentPeriod(int $period_num)
    {
        // TODO: check if it exists first. Throw Exception
        return $this->paymentPeriods()[$period_num];
    }

    public function cumulativePeriodsForPeriod(int $period_num)
    {
        return $this->paymentPeriod($period_num)->cumulativePeriods();
    }

    public function cumulativePartialDaysForPeriod(int $period_num)
    {
        return $this->paymentPeriod($period_num)->cumulativePartialDays();
    }

    public function cumulativePartialPeriodRatioForPeriod(int $period_num)
    {
        return $this->paymentPeriod($period_num)->cumulativePartialDays() / $this->daysInPeriod();
    }

    public function numberOfPeriods()
    {
        return count($this->paymentPeriods());
    }
}
