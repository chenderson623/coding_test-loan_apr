<?php

namespace LoanCodingTest;

interface LoanInterest
{
    public function annualInterestRate();
    public function ratePerPeriod();
}
