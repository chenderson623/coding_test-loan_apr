<?php

namespace LoanCodingTest;

/**
 * Generic iterator. Works by generating 2 answers and calculating a new guess
 * Repeats until the desired answer is reached.
 */
class Iterator
{
    // The presision to compare answer to desired answer
    protected $answer_precision;

    // This is our iteration step. 0.1 seems to be standard
    protected $interpolation_step;

    // Limit number of iterations - so we don't get stuck in an infinite loop
    protected $iteration_limit = 1000;

    public function __construct($answer_precision = 2, $interpolation_step = 0.1)
    {
        $this->answer_precision   = $answer_precision;
        $this->interpolation_step = $interpolation_step;
    }

    /**
     * This is the public interface
     *
     * @param      callable        $function        The function to iterate
     * @param      int|float       $desired_answer  The desired answer
     * @param      int|float       $guess           The guess
     *
     * @return     IteratorResult  Returns an IteratorResult so that we can also see the number of iterations and the last answer calculated
     */
    public function iterate(callable $function, $desired_answer, $guess)
    {
        // Return a result class, so we can pass back multiple parameters
        $result = new IteratorResult();

        $result->answer            = $guess;
        $result->calculated_result = $function($guess);

        while (round($result->calculated_result, $this->answer_precision) !== round($desired_answer, $this->answer_precision)) {
            $result->iteration_count++;

            // get our next guess:
            $result->answer = $this->calculateNextGuess(
                $function,
                $result->answer,
                $result->calculated_result,
                $desired_answer
            );

            $result->calculated_result = $function($result->answer);

            if ($result->iteration_count > $this->iteration_limit) {
                return $result;
            }
        }

        return $result;
    }

    /**
     * Calculates the next guess.
     *
     * @param      callable    $function            The function
     * @param      int|float   $guess1              The guess 1
     * @param      int|float   $calculated_result1  The calculated result 1
     * @param      int|float   $desired_answer      The desired answer
     *
     * @return     float   The next guess.
     */
    protected function calculateNextGuess(callable $function, $guess1, $calculated_result1, $desired_answer)
    {
        $guess2 = $guess1 + $this->interpolation_step;

        $calculated_result2 = $function($guess2);

        // return next guess
        return $guess1 + ($this->interpolation_step * ( ($desired_answer - $calculated_result1) / ($calculated_result2 - $calculated_result1) ));
    }
}

class IteratorResult
{
    public $iteration_count = 0;
    public $answer;
    public $calculated_result;
    public $completed = false;
}
