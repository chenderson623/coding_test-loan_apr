
#2rd version of coding test.

###Components
* **Loan:** loan consists of a loan amount, an interest object, and a payment schedule object
    - attempted to encapsulate what consists a loan, and delegate out what can change
    - Loan should be a representation of a loan, but not get bogged down with financial calculations
* **LoanInterest:** component of a Loan. Allows the use of other interest types in a Loan
    - encapsulates what can change in an interest calculation: interest type (simple, compounding, other), other components that affect interest: compounding period, points
    - uses LoanInterest and PaymentSchedule interfaces as strategies
* **PaymentSchedule:** Holds our calculated payment period intervals
    - encapsulates the calculation of payment period whole periods/partial periods. A normal period is defined here
* **FinancialFormula:** Our financial calculation library. Separated for use in other code
* **PaymentPeriodFactory:** Re-usable code to generate PaymentPeriod objects
    - PayPeriods COULD calculate cumulative periods themselves, but each would have to know the origination date and days in a normal period APRCalculation: This is the meat of the problem. Calculates actual APR by interpolating from an initial guess. Tests a given APR by calculating payments to principal and iterating until the calculated payments equal the loan's loan amount
* **Iterator:** A generic iterator that should be reusable

##Use case
```php
     // Define our parameters:
     $loan_amount = 300;
     $interest_rate = 6.5;
     $origination_date = new DateTimeImmutable("2018-01-25");
     $payment_dates = [
         new DateTimeImmutable('2018-02-02'),
         new DateTimeImmutable('2018-02-16'),
         new DateTimeImmutable('2018-03-02'),
         new DateTimeImmutable('2018-03-16'),
         new DateTimeImmutable('2018-03-30'),
         new DateTimeImmutable('2018-04-13'),
         new DateTimeImmutable('2018-04-27'),
         new DateTimeImmutable('2018-05-11'),
         new DateTimeImmutable('2018-05-25'),
         new DateTimeImmutable('2018-06-08'),
         new DateTimeImmutable('2018-06-22'),
         new DateTimeImmutable('2018-07-06')
     ];
      $loan = new Loan(
         $loan_amount,
         new AnnualInterestCompoundedBiWeekly($interest_rate),
         new BiWeeklyPaymentSchedule($origination_date, $payment_dates)
     );
      $apr = $loan->calculatedApr();
      print $apr . PHP_EOL;
```

##Output:
```
  Iterations: 4
  7.3304
```
###Possible Improvements
1. Here we are putting origination date in the PaymentSchedule. Needed to calculate period days, but doesn't logically fit as a "payment". PaymentSchedule might be re-nameable to something like "LoanSchedule"
2. Creation of APRCalculation is hard-coded in Loan. Might be better to inject it as a dependency to Loan.
  * Problem is that APRCalculation is dependent on both LoanInterest and PaymentSchedule.
  * The calculation made in APRCalculation is probably invalid for other LoanInterest types.
3. Other loan parameters affect APR. Like: fees, points
4. Need to improve error checking. Throw custom Exceptions
