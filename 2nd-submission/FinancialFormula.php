<?php

namespace LoanCodingTest;

/**
 * Library of financial calculations for reuse.
 * These should all be static functions
 */
class FinancialFormula
{
    /**
     * Rate per period formula
     *
     * @param float   $I  annual interest
     * @param float   $n  number of compounding periods/year
     * @param float   $p  number of payment periods per year. if null, is equal to $n
     *
     * @return float rate per period (R)
     */
    public static function ratePerPeriod(float $I, float $n, float $p = null)
    {
        if (empty($p)) {
            $p = $n;
        }
        $R = (1 + ($I / $n))**($n/$p) - 1;
        return $R;
    }

    /**
     * Standard formula for fixed periodic payments for compounding interest loans:
     *  P = (iA) / (1 - (1 + i)^-N)
     *
     * @param      float    $A      loan amount
     * @param      float    $i      interest rate per period (annual interest / interest periods)
     * @param      integer  $N      total number of payments
     *
     * @return     integer  payment amount (P)
     */
    public static function paymentAmount(float $A, float $i, int $N)
    {
        return ($i * $A) / (1 - (1 + $i)**-$N);
    }

    /**
     * This APPEARS to be an inverted series of the principal payment on an amortization schedule
     * TODO: verify name of this function
     *
     * @param      float    $P                     payment amount
     * @param      float    $R                     rate per period
     * @param      integer  $n                     period number
     * @param      float    $partial_period_ratio  partial period ratio (optional)
     *
     * @return     integer  ( description_of_the_return_value )
     */
    public static function amortizedPrincipalForPeriod($P, $R, $n, $partial_period_ratio = 0)
    {
        return $P / ((1 + ($partial_period_ratio * $R)) * (1 + $R)**$n );
    }
}
