<?php

namespace LoanCodingTest;

/**
 * Class for annual interest compounded bi weekly (26 times a year).
 */
class AnnualInterestCompoundedBiWeekly implements LoanInterest
{
    protected $annual_interest_periods = 26;
    protected $annual_interest_rate;

    public function __construct($annual_interest_rate)
    {
        $this->annual_interest_rate = (float) $annual_interest_rate;
    }

    public function annualInterestRate()
    {
        return $this->annual_interest_rate;
    }

    public function ratePerPeriod()
    {
        return FinancialFormula::ratePerPeriod($this->annual_interest_rate, $this->annual_interest_periods);
    }
}
