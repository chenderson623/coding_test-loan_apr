<?php

namespace LoanCodingTest;

use DateTimeImmutable;

/**
 * Re-usable code to generate PaymentPeriod objects
 *   PayPeriod objects COULD calculate cumulative periods themselves, but each
 * would have to know the origination date and days in a normal period
 */
class PaymentPeriodFactory
{
    protected $origination_date;
    protected $days_in_normal_period;

    public function __construct(DateTimeImmutable $origination_date, $days_in_normal_period)
    {
        $this->origination_date      = $origination_date;
        $this->days_in_normal_period = (int) $days_in_normal_period;
    }

    public function cumulativePartialDaysForPeriod($period_number, DateTimeImmutable $payment_date)
    {
        $days_if_normal_periods         = $this->days_in_normal_period * $period_number;
        $payment_date_if_normal_periods = $this->origination_date->modify('+' . $days_if_normal_periods . 'days');
        /**
         * @var DateInterval
         */
        $actual_payment_date_diff       = $payment_date_if_normal_periods->diff($payment_date);
        $partial_days                   = (int) $actual_payment_date_diff->format('%r%a');
        return $partial_days;
    }

    /**
     * Normlize so that partial days are positive numbers and less than a full period
     *
     * @param      integer  $partial_days
     * @param      integer  $whole_periods
     *
     * @return     array   [partial_days, whole_periods]
     */
    public function normalizePartialDays($partial_days, $whole_periods)
    {
        // have enough days for another period:
        while ($partial_days > $this->days_in_normal_period) {
            $partial_days = $partial_days - $this->days_in_normal_period;
            $whole_periods++;
        }
        // negative partial days: take from whole period
        while ($partial_days < 0) {
            $partial_days = $partial_days + $this->days_in_normal_period;
            $whole_periods--;
        }
        return [$partial_days, $whole_periods];
    }

    public function paymentPeriod($period_number, DateTimeImmutable $payment_date)
    {
        $cumulative = $this->normalizePartialDays(
            $this->cumulativePartialDaysForPeriod((int) $period_number, $payment_date),
            (int) $period_number
        );

        return new PaymentPeriod(
            $period_number,
            $payment_date,
            $cumulative[1],
            $cumulative[0]
        );
    }

    public function paymentPeriods($payment_schedule)
    {
        $payment_periods = [];
        // payment schedule should be 1-based array
        $payment_num = 1;
        foreach ($payment_schedule as $payment_date) {
            $payment_periods[$payment_num] = $this->paymentPeriod($payment_num, $payment_date);
            $payment_num++;
        }
        return $payment_periods;
    }
}
