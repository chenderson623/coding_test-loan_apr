<?php

namespace LoanCodingTest;

/**
 * This is the princial definition point of the problem. Here we
 * define a Loan with a loan amount, an interest object, and a
 * payment schedule object.
 *
 * Also provides access to the APR calculation
 */
class Loan
{
    /**
     * @var float
     */
    protected $loan_amount;
    /**
     * @var LoanInterest
     */
    protected $loan_interest;
    /**
     * @var PaymentSchedule
     */
    protected $payment_schedule;

    public function __construct($loan_amount, LoanInterest $loan_interest, PaymentSchedule $payment_schedule)
    {
        $this->loan_amount      = round((float) $loan_amount, 2);
        $this->loan_interest    = $loan_interest;
        $this->payment_schedule = $payment_schedule;
    }

    public function loanAmount()
    {
        return $this->loan_amount;
    }

    public function loanInterest()
    {
        return $this->loan_interest;
    }

    public function paymentSchedule()
    {
        return $this->payment_schedule;
    }

    /**
     * calculate the periodic payment amount
     *
     * @return     float
     */
    public function paymentAmount()
    {
        // could cache this if called frequently to avoid repeated calculation
        return FinancialFormula::paymentAmount(
            $this->loan_amount,
            $this->loanInterest()->ratePerPeriod(),
            $this->paymentSchedule()->numberOfPeriods()
        );
    }

    /**
     * Create an APRCalculation object and return the apr
     *
     * @return     float
     */
    public function calculatedApr()
    {
        return (new APRCalculation(
            $this,
            $this->loanInterest(),
            $this->paymentSchedule()
        ))->apr();
    }
}
