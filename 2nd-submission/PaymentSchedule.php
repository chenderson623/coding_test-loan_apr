<?php

namespace LoanCodingTest;

interface PaymentSchedule
{
    public function paymentPeriods();
    public function daysInPeriod();
    public function paymentPeriod(int $period_num);
    public function cumulativePeriodsForPeriod(int $period_num);
    public function cumulativePartialDaysForPeriod(int $period_num);
    public function cumulativePartialPeriodRatioForPeriod(int $period_num);
    public function numberOfPeriods();
}
