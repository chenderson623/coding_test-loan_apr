<?php

$interest_rate = 6.5;
$origination_date="1/25/2018";
$payment_schedule=array();
$payment_schedule[1] = '2018-02-02';
$payment_schedule[2] = '2018-02-16';
$payment_schedule[3] = '2018-03-02';
$payment_schedule[4] = '2018-03-16';
$payment_schedule[5] = '2018-03-30';
$payment_schedule[6] = '2018-04-13';
$payment_schedule[7] = '2018-04-27';
$payment_schedule[8] = '2018-05-11';
$payment_schedule[9] = '2018-05-25';
$payment_schedule[10] = '2018-06-08';
$payment_schedule[11] = '2018-06-22';
$payment_schedule[12] = '2018-07-06';


function apr($apr_guess, $interest_rate, $payment_schedule, $origination_date)
{
    $whole_periods = [];
    $extra_days = array();
    $loan_amount = 300;
    $number_of_payments = count($payment_schedule);
    $interest_periods = 26;
    $unit_period = 14;
    $payment_amount = $loan_amount * (($interest_rate / $interest_periods) / (1 - pow((1 + ($interest_rate / $interest_periods)), -$number_of_payments)));
    $period_apr = $apr_guess / (365 / $unit_period);

    for ($x = 1; $x <= $number_of_payments; $x++) {
        $whole_periods[$x] = $x;
        $extra_days[$x] = (strtotime(date($payment_schedule[$x])) - strtotime($origination_date . " + " . $unit_period * ($x) . " days")) / (86400);

        while ($extra_days[$x] > $unit_period) {
            $extra_days[$x] = $extra_days[$x] - $unit_period;
            $whole_periods[$x]++;
        }
        while ($extra_days[$x] < 0) {
            $extra_days[$x] = $extra_days[$x] + $unit_period;
            $whole_periods[$x]--;
        }
    }

    $calculated_apr = -$loan_amount;
    for ($i = 1; $i <= $number_of_payments; $i++) {
        $calculated_apr += $payment_amount / ((1 + (($extra_days[$i] / $unit_period) * $period_apr)) * (pow((1 + $period_apr), $whole_periods[$i])));
    }

    return $calculated_apr;
}

/**
 * This is our generic iteration next-guess function
 * We pass answer1 here instead of calculating it because we need to calculate an answer in our loop anyway
 *
 * @param      callable  $function            The function we are calling. Should take 1 argument function(guess)
 * @param      float     $guess1              The guess we used for answer1
 * @param      float     $answer1             The answer1 we calculated elsewhere
 * @param      float     $interpolation_step  The amount of step to make for the next guess
 * @param      float     $desired_answer      The answer we are trying to get to
 *
 * @return     float     The next guess.
 */
function calculateNextGuess(callable $function, $guess1, $answer1, $interpolation_step, $desired_answer)
{
    $guess2 = $guess1 + $interpolation_step;

    $answer2 = $function($guess2);

    // return next guess
    return $guess1 + ($interpolation_step * ( ($desired_answer - $answer1) / ($answer2 - $answer1) ));
}

/**
 * Our iteration function, generated as a partial
 * The function we want to iterate is function($guess)
 * $interest_rate, $payment_schedule, $origination_date remain constant
 *
 * @param      float   $interest_rate     The interest rate
 * @param      array   $payment_schedule  The payment schedule
 * @param      string  $origination_date  The origination date
 *
 * @return     float the over/under payment of principal
 */
$principalOverpaymentPartialFunc = function ($interest_rate, $payment_schedule, $origination_date) {
    return function ($guess) use ($interest_rate, $payment_schedule, $origination_date) {
        return apr($guess, $interest_rate, $payment_schedule, $origination_date);
    };
};

/**
 * This is our iterator function
 * The apr function is returning the over/under payment of principal. We want this to be 0
 *
 * @param      float     $guess                     The guess
 * @param      callable  $principalOverpaymentFunc  The principal overpayment function
 *
 * @return     float    The APR
 */
function iterateApr($guess, callable $principalOverpaymentFunc)
{
    $apr = $guess;
    // This is our iteration step. 0.1 seems to be standard
    $interpolation_step = 0.1;
    // We want the over/under payment of principal to be 0
    $desired_answer     = 0;

    // count how many iterations we make
    $count = 0;
    do {
        $principalOverpayment = $principalOverpaymentFunc($apr);
        $count++;
        // get our next guess:
        $apr = calculateNextGuess($principalOverpaymentFunc, $apr, $principalOverpayment, $interpolation_step, $desired_answer);
    // Stop when our answer comes out to $0.00 (2 decimal places)
    } while (abs(round($principalOverpayment, 2)) > 0);
    print "Iterations: $count" . PHP_EOL;
    return $apr;
}

// Call like this:
$apr_guess = 6.5;
$apr       = iterateApr($apr_guess, $principalOverpaymentPartialFunc($interest_rate, $payment_schedule, $origination_date));

print "APR: $apr" . PHP_EOL;
