Please complete the function below, in PHP.  The goal is to create a function that calculates APR when provided with Interest Rate, Origination Date, Payment Schedule (array of payment dates) and Loan Amount.

This function must be able to calculate an APR for installment loans with irregular periods, meaning the length of time between each payment is not always a full interest period.  Often, each payment is a few days more or less than a whole interest period from the start of the loan. To solve for the APR, you will need to utilize an iterative approach. Your equation should start with a variable that contains an estimated APR. You will then continue solving the APR equation and adjusting the estimated APR until your estimated APR converges on the correct answer (Y(i) = 0).

There is partially completed PHP code below, you can copy it into the edit box and make your changes.  The code is missing one section that iterates through the APR function to find the correct value. Also, there are some structural issues with the code below and it’s not set up in an object-oriented way.

You should complete the problem below and provide clean, well-structured, well-commented code. (The editor defaults to Python, you probably want to change it over to PHP)

//————————————————
$interest_rate = 6.5;
$origination_date="1/25/2018";
$payment_schedule=array();
$payment_schedule[1] = '2018-02-02';
$payment_schedule[2] = '2018-02-16';
$payment_schedule[3] = '2018-03-02';
$payment_schedule[4] = '2018-03-16';
$payment_schedule[5] = '2018-03-30';
$payment_schedule[6] = '2018-04-13';
$payment_schedule[7] = '2018-04-27';
$payment_schedule[8] = '2018-05-11';
$payment_schedule[9] = '2018-05-25';
$payment_schedule[10] = '2018-06-08';
$payment_schedule[11] = '2018-06-22';
$payment_schedule[12] = '2018-07-06';


/**
* Code here to iterate through testing different apr values until we converge on the right answer. Right answer is: 7.3303
*
*/

//correct example - (print(round(apr(7.3303,$interest_rate,$payment_schedule,$origination_date),2));


function apr($apr_guess,$interest_rate,$payment_schedule,$origination_date)
{
    $whole_periods = [];
    $extra_days = array();
    $loan_amount = 300;
    $number_of_payments = count($payment_schedule);
    $interest_periods = 26;
    $unit_period = 14;
    $payment_amount = $loan_amount * (($interest_rate / $interest_periods) / (1 - pow((1 + ($interest_rate / $interest_periods)), -$number_of_payments)));
    $period_apr = $apr_guess / (365 / $unit_period);

    for ($x = 1; $x <= $number_of_payments; $x++) {
        $whole_periods[$x] = $x;
        $extra_days[$x] = (strtotime(date($payment_schedule[$x])) - strtotime($origination_date . " + " . $unit_period * ($x) . " days")) / (86400);

        while ($extra_days[$x] > $unit_period) {
            $extra_days[$x] = $extra_days[$x] - $unit_period;
            $whole_periods[$x]++;
        }
        while ($extra_days[$x] < 0) {
            $extra_days[$x] = $extra_days[$x] + $unit_period;
           $whole_periods[$x]--;
        }
    }

    $calculated_apr = -$loan_amount;
    for ($i = 1; $i <= $number_of_payments; $i++) {
        $calculated_apr += $payment_amount / ((1 + (($extra_days[$i] / $unit_period) * $period_apr)) * (pow((1 + $period_apr), $whole_periods[$i])));
    }

    return $calculated_apr;

}
//————————————————