<?php

// this is really 650%? by my experiments, should be passing .065 to $payment_amount
// running the problem with .065 and .065 as a guess, results in 0.0698 (6.98%), not 7.3303
// I'm going to assume that 650% is the correct interest rate for this problem
$interest_rate = 6.5;

// probably be good to pass a Date object
$origination_date="1/25/2018";
$payment_schedule=array();
$payment_schedule[1] = '2018-02-02';
$payment_schedule[2] = '2018-02-16';
$payment_schedule[3] = '2018-03-02';
$payment_schedule[4] = '2018-03-16';
$payment_schedule[5] = '2018-03-30';
$payment_schedule[6] = '2018-04-13';
$payment_schedule[7] = '2018-04-27';
$payment_schedule[8] = '2018-05-11';
$payment_schedule[9] = '2018-05-25';
$payment_schedule[10] = '2018-06-08';
$payment_schedule[11] = '2018-06-22';
$payment_schedule[12] = '2018-07-06';


function apr($apr_guess, $interest_rate, $payment_schedule, $origination_date)
{
    // these can be combined to array of [
    //    // "extra" days is probably not right. better "partial" days. In our example, there are no extra days
    //    // the first period is actually short days
    //    'partial_days' =>
    //    'periods' => 0,
    // ]
    $whole_periods = [];
    $extra_days = array();

    // Belongs to Loan object.
    $loan_amount = 300;
    $number_of_payments = count($payment_schedule);

    // Number of periods per year. This is a compunding argument. This belongs to an interest object
    $interest_periods = 26;

    // This is used to calculate proportion of whole periods. This belongs to a payment schedule object
    // rename: normal_period_days
    $unit_period = 14;

    // This is that standard payment formula for compounding interest loans:
    //   P = (iA) / (1 - (1 + i)^N)
    //      P: payment
    //      A: loan amount
    //      i: interest rate per period (anual interest / interest periods)
    //      N: total number of payments
    //   PHP format: ($i * $A) / (1 - (1 + $i)**$N)
    //   put this in a finanacial calculation library
    $payment_amount = $loan_amount * (($interest_rate / $interest_periods) / (1 - pow((1 + ($interest_rate / $interest_periods)), -$number_of_payments)));

    // This is a derrivation of the rate per period formula:
    // R=(1 + (I/n))^(n/p) - 1
    //   I = annual interest
    //   n = number of compunding periods/year
    //   p = number of payment periods per year
    // in this case, n & p are equal, and 365/unit_period interest periods = 26
    // put this in a finanacial calculation library
    $period_apr = $apr_guess / (365 / $unit_period);

    for ($x = 1; $x <= $number_of_payments; $x++) {
        $whole_periods[$x] = $x;

        // difference between actual payment date and date if we had whole periods
        // payment_date - (origination_date + (period * normal_period_days))
        // -6 in our example case. first period is 6 days short of a full period
        // Clearer to use DateTime objects?
        //    $days_for_normal_periods = $unit_period * ($x);
        //    $date_if_normal_periods = $origination_date->modify('+' . $days_for_normal_periods . 'days');
        //    $payment_diff_interval = $payment_date->diff($date_if_normal_periods)
        //    $extra_days = (int) $payment_diff->format('%r%a')
        $extra_days[$x] = (strtotime(date($payment_schedule[$x])) - strtotime($origination_date . " + " . $unit_period * ($x) . " days")) / (86400);

        // Normalizing extra days to whole periods if extra days negative or extra_days > unit_period
        while ($extra_days[$x] > $unit_period) {
            $extra_days[$x] = $extra_days[$x] - $unit_period;
            $whole_periods[$x]++;
        }
        while ($extra_days[$x] < 0) {
            $extra_days[$x] = $extra_days[$x] + $unit_period;
            $whole_periods[$x]--;
        }
    }

    $calculated_apr = -$loan_amount;
    for ($i = 1; $i <= $number_of_payments; $i++) {
        // This APPEARS to be an inverted series of the principal payment on an ammoration schedule
        // for a guess of 7.3303 the formula produces:
        // double(69.386242206045)
        // double(54.158665404987)
        // double(42.272948428872)
        // double(32.995683248604)
        // double(25.754416323102)
        // double(20.102325360143)
        // double(15.690648151967)
        // double(12.247162207261)
        // double(9.5593872654755)
        // double(7.4614742047881)
        // double(5.8239713239559)
        // double(4.54583652658)
        // TOTAL:
        // double(299.99876065178)
        // I can't find any derrivation of this
        // might clarify this; that we are returning "overpayment" of principal
        // Convert this to same nomenclature as above:
        // $P / ((1 + (($days / $normal_period_days) * $R)) * (1 + $R)**$periods )
        $calculated_apr += $payment_amount / ((1 + (($extra_days[$i] / $unit_period) * $period_apr)) * (pow((1 + $period_apr), $whole_periods[$i])));
    }

    return $calculated_apr;
}

/*
Problem breakdown:

Objects:
    Loan: takes: amount, interest, payment schedule, origination date
        what can change:
            - interest type: could be simple interes, compound interest
            - compounding periods
    LoanInterest:
*/
